import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { Product } from './model/products';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  allProducts: Product[] = []
  api_address = NG_APP_API

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.fetchProducts()
  }

  onProductsFetch() {
    this.fetchProducts()
  }

  onProductCreate(products: { pName: string, desc: string, price: string }) {
    console.log(products)
    const headers = new HttpHeaders({ 'myHeader': 'blabla' });
    this.http.post<{name: string}>('https://compact-life-308412-default-rtdb.europe-west1.firebasedatabase.app/products.json', products, { headers: headers }).subscribe((res) => {
      console.log(res)
    });
  }

  private fetchProducts() {
    this.http.get<{[key: string]: Product }>('https://compact-life-308412-default-rtdb.europe-west1.firebasedatabase.app/products.json')
      .pipe(map((res) => {
        const products = []
        for (const key in res) {
          if (res.hasOwnProperty(key)) {
            products.push({ ...res[key], id: key })
          }
        }
        return products
      })
      ).subscribe((products) => {
        console.log(products)
        this.allProducts = products;
      })
  }

  onDeleteProduct(id: string){
    this.http.delete('https://compact-life-308412-default-rtdb.europe-west1.firebasedatabase.app/products/'+id+'.json').subscribe()
    console.log(this.api_address)
  }

  onDeleteAllProducts(){
    this.http.delete('https://compact-life-308412-default-rtdb.europe-west1.firebasedatabase.app/products.json/').subscribe()
  }

}
